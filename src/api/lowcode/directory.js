import request from '@/utils/request'

// 查询目录列表
export function listDirectory(query) {
  return request({
    url: '/lowcode/directory/list',
    method: 'get',
    params: query
  })
}

// 查询目录详细
export function getDirectory(directoryId) {
  return request({
    url: '/lowcode/directory/' + directoryId,
    method: 'get'
  })
}

// 新增目录
export function addDirectory(data) {
  return request({
    url: '/lowcode/directory',
    method: 'post',
    data: data
  })
}

// 修改目录
export function updateDirectory(data) {
  return request({
    url: '/lowcode/directory',
    method: 'put',
    data: data
  })
}

// 删除目录
export function delDirectory(directoryId) {
  return request({
    url: '/lowcode/directory/' + directoryId,
    method: 'delete'
  })
}
