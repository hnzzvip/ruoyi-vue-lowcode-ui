import request from '@/utils/request'

export function selectListSql(data) {
    return request({
      url: '/lowcode/selectListSql',
      method: 'post',
      data: data
    })
}

export function testConnection(data) {
  return request({
    url: '/lowcode/testConnection',
    method: 'post',
    data: data
  })
}