import request from '@/utils/request'

// 查询模型列表
export function listModel(query) {
  return request({
    url: '/lowcode/model/list',
    method: 'get',
    params: query
  })
}

export function queryModelList(query) {
  return request({
    url: '/lowcode/model/queryList',
    method: 'get',
    params: query
  })
}

// 获取模型下面子模型
export function getModelSon(modelId) {
  return request({
    url: '/lowcode/model/getModelSon/' + modelId,
    method: 'get'
  })
}

// 获取模型搜索参数，列表参数信息
export function getTableModel(modelId) {
  return request({
    url: '/lowcode/model/table/' + modelId,
    method: 'get'
  })
}

/** 根据模型id获取数据列表 */
export function selectListByModel(data){
  return request({
    url: '/lowcode/model/selectListByModel',
    method: 'post',
    data: data
  })
}

// 初始化模型字段
export function initializationField(modelId) {
  return request({
    url: '/lowcode/model/initializationField/' + modelId,
    method: 'get'
  })
}

// 查询模型详细
export function getModel(modelId) {
  return request({
    url: '/lowcode/model/' + modelId,
    method: 'get'
  })
}

// 新增或修改模型数据
export function addOrUpdateModelRow(data) {
  return request({
    url: '/lowcode/model/addOrUpdateModelRow',
    method: 'post',
    data: data
  })
}

// 删除模型数据
export function deleteModelRow(data) {
  return request({
    url: '/lowcode/model/deleteModelRow',
    method: 'post',
    data: data
  })
}

// 新增模型
export function addModel(data) {
  return request({
    url: '/lowcode/model',
    method: 'post',
    data: data
  })
}

// 修改模型
export function updateModel(data) {
  return request({
    url: '/lowcode/model',
    method: 'put',
    data: data
  })
}

// 删除模型
export function delModel(modelId) {
  return request({
    url: '/lowcode/model/' + modelId,
    method: 'delete'
  })
}
