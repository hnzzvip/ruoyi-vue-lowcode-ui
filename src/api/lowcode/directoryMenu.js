import request from '@/utils/request'

// 查询目录菜单列表
export function listDirectoryMenu(query) {
  return request({
    url: '/lowcode/directoryMenu/list',
    method: 'get',
    params: query
  })
}

// 查询目录菜单详细
export function getDirectoryMenu(menuId) {
  return request({
    url: '/lowcode/directoryMenu/' + menuId,
    method: 'get'
  })
}

// 新增目录菜单
export function addDirectoryMenu(data) {
  return request({
    url: '/lowcode/directoryMenu',
    method: 'post',
    data: data
  })
}

// 修改目录菜单
export function updateDirectoryMenu(data) {
  return request({
    url: '/lowcode/directoryMenu',
    method: 'put',
    data: data
  })
}

// 删除目录菜单
export function delDirectoryMenu(menuId) {
  return request({
    url: '/lowcode/directoryMenu/' + menuId,
    method: 'delete'
  })
}
