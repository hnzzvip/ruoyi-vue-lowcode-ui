import request from '@/utils/request'

// 查询数据字典列表
export function listDictionary(query) {
  return request({
    url: '/lowcode/dictionary/list',
    method: 'get',
    params: query
  })
}

// 查询数据字典列表全部数据
export function listDictionaryAll() {
  return request({
    url: '/lowcode/dictionary/queryList',
    method: 'get'
  })
}

// 查询数据字典详细
export function getDictionary(dictionaryId) {
  return request({
    url: '/lowcode/dictionary/' + dictionaryId,
    method: 'get'
  })
}

// 新增数据字典
export function addDictionary(data) {
  return request({
    url: '/lowcode/dictionary',
    method: 'post',
    data: data
  })
}

// 修改数据字典
export function updateDictionary(data) {
  return request({
    url: '/lowcode/dictionary',
    method: 'put',
    data: data
  })
}

// 删除数据字典
export function delDictionary(dictionaryId) {
  return request({
    url: '/lowcode/dictionary/' + dictionaryId,
    method: 'delete'
  })
}
