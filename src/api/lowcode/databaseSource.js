import request from '@/utils/request'

// 查询数据源列列表
export function listDatabaseSource(query) {
  return request({
    url: '/lowcode/databaseSource/list',
    method: 'get',
    params: query
  })
}

// 查询数据源列详细
export function getDatabaseSource(databaseSourceId) {
  return request({
    url: '/lowcode/databaseSource/' + databaseSourceId,
    method: 'get'
  })
}

// 新增数据源列
export function addDatabaseSource(data) {
  return request({
    url: '/lowcode/databaseSource',
    method: 'post',
    data: data
  })
}

// 修改数据源列
export function updateDatabaseSource(data) {
  return request({
    url: '/lowcode/databaseSource',
    method: 'put',
    data: data
  })
}

// 删除数据源列
export function delDatabaseSource(databaseSourceId) {
  return request({
    url: '/lowcode/databaseSource/' + databaseSourceId,
    method: 'delete'
  })
}

export function databaseSourceType() {
  return request({
    url: '/lowcode/databaseSource/type',
    method: 'get'
  })
}

