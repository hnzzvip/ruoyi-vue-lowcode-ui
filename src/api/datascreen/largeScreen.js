import request from '@/utils/request'

// 查询大屏信息列表
export function listLargeScreen(query) {
  return request({
    url: '/datascreen/largeScreen/list',
    method: 'get',
    params: query
  })
}

// 查询大屏信息详细
export function getLargeScreen(largeScreenId) {
  return request({
    url: '/datascreen/largeScreen/' + largeScreenId,
    method: 'get'
  })
}

// 新增大屏信息
export function addLargeScreen(data) {
  return request({
    url: '/datascreen/largeScreen',
    method: 'post',
    data: data
  })
}

// 修改大屏信息
export function updateLargeScreen(data) {
  return request({
    url: '/datascreen/largeScreen',
    method: 'put',
    data: data
  })
}

// 删除大屏信息
export function delLargeScreen(largeScreenId) {
  return request({
    url: '/datascreen/largeScreen/' + largeScreenId,
    method: 'delete'
  })
}
