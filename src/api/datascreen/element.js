import request from '@/utils/request'

// 查询大屏元素列表
export function listElement(query) {
  return request({
    url: '/datascreen/element/list',
    method: 'get',
    params: query
  })
}

// 查询大屏元素详细
export function getElement(elementId) {
  return request({
    url: '/datascreen/element/' + elementId,
    method: 'get'
  })
}

// 查询大屏元素列表 全部数据
export function listAllElement(query) {
  return request({
    url: '/datascreen/element/listAll',
    method: 'get',
    params: query
  })
}

// 查询大屏元素详细
export function getElementForUpdate(elementId) {
  return request({
    url: '/datascreen/element/info/' + elementId,
    method: 'get'
  })
}

// 查询大屏元素数据
export function getElementForEcharts(elementId) {
  return request({
    url: '/datascreen/element/echarts/' + elementId,
    method: 'get'
  })
}

// 新增大屏元素
export function addElement(data) {
  return request({
    url: '/datascreen/element',
    method: 'post',
    data: data
  })
}

// 修改大屏元素
export function updateElement(data) {
  return request({
    url: '/datascreen/element',
    method: 'put',
    data: data
  })
}

// 删除大屏元素
export function delElement(elementId) {
  return request({
    url: '/datascreen/element/' + elementId,
    method: 'delete'
  })
}
