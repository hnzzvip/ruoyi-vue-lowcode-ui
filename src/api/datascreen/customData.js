import request from '@/utils/request'

// 查询自定义数据集列表
export function listCustomData(query) {
  return request({
    url: '/datascreen/customData/list',
    method: 'get',
    params: query
  })
}

// 查询全部自定义数据集列表
export function listAllCustomData(query) {
  return request({
    url: '/datascreen/customData/getAll',
    method: 'get',
    params: query
  })
}

// 查询自定义数据集详细
export function getCustomData(customId) {
  return request({
    url: '/datascreen/customData/' + customId,
    method: 'get'
  })
}

// 新增自定义数据集
export function addCustomData(data) {
  return request({
    url: '/datascreen/customData',
    method: 'post',
    data: data
  })
}

// 修改自定义数据集
export function updateCustomData(data) {
  return request({
    url: '/datascreen/customData',
    method: 'put',
    data: data
  })
}

// 删除自定义数据集
export function delCustomData(customId) {
  return request({
    url: '/datascreen/customData/' + customId,
    method: 'delete'
  })
}
